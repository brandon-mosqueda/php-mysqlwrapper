CREATE DATABASE test;

USE test;

CREATE TABLE users(
  userId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(250) NULL,
  email VARCHAR(250) NULL
);

INSERT INTO users (name, email)
VALUES ('Brandon Mosqueda', 'brandon16mg@gmail.com'),
       ('Alejandro Gonzalez', 'alejandro@hotmail.com'),
       ('Lucienne Monti', 'Monti@gmail.com');