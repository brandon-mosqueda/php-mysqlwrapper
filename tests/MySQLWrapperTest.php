<?php
/**
 * MySQLWrapper - An easy way to query securitely MySQL databases.
 * PHP Version 7.3
 *
 * @see         https://gitlab.com/brandon16mg/php-mysqlwrapper
 * 
 * @author      Brandon Mosqueda (https://gitlab.com/brandon16mg)
 * @copyright   2019 Brandon Mosqueda
 * @license     MIT
 *
 * @version     1
 */

if(!defined('SITE_ROOT')) {
  define('SITE_ROOT', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
}

require SITE_ROOT . 'MySQLWrapper.php';

use PHPUnit\Framework\TestCase;

error_reporting(0);

class MySQLWrapperTest extends TestCase
{
    private function setCredentials()
    {
        MySQLWrapper::$password = 'mariaPass';
        MySQLWrapper::$database = 'test';
    }

    private function getAllUsers()
    {
        return MySQLWrapper::getArrayBySql("SELECT * FROM users");
    }

    private function getAllUsersNum()
    {
        return MySQLWrapper::getCountBySql("SELECT COUNT(*) FROM users");
    }

    private function getFooObjectFromDatabase()
    {
        return MySQLWrapper::getObjectBySql("SELECT 'foo' AS name, 3 AS value");   
    }

    private function insertUserWithClassicBinding($values)
    {
        return MySQLWrapper::query(
            "INSERT INTO users (name, email)
             VALUES (?, ?)",
            'ss',
            $values
        );
    }

    private function insertUserWithCustomBinding($values)
    {
        return MySQLWrapper::query(
            "INSERT INTO users (name, email)
             VALUES ({name}, {email})",
            'ss',
            $values
        );
    }

    private function getUserByIdWithClassicBinding($id)
    {
        return MySQLWrapper::getObjectBySql(
            "SELECT * FROM users WHERE userId = ?",
            'i',
            [$id]
        );
    }

    private function getUserByIdWithCustomBinding($param)
    {
        return MySQLWrapper::getObjectBySql(
            "SELECT * FROM users WHERE userId = {userId}",
            'i',
            $param
        );
    }

    private function deleteAllUsers()
    {
        return MySQLWrapper::query("DELETE FROM users");
    }

    public function testCorrectSelectsWithoutParams()
    {
        $this->setCredentials();

        $users = $this->getAllUsers();

        $this->assertNotEmpty($users);

        $usersNum = $this->getAllUsersNum();

        $this->assertIsInt($usersNum);

        $obj = $this->getFooObjectFromDatabase();

        $this->assertObjectHasAttribute('name', $obj);
        $this->assertObjectHasAttribute('value', $obj);
    }

    public function testCorrectQueriesWithClassicBinding()
    {
        $this->setCredentials();

        $result = $this->insertUserWithClassicBinding(['foo', 'bar@foo.com']);

        $this->assertObjectHasAttribute('insert_id', $result);
        $this->assertObjectHasAttribute('affected_rows', $result);
        $this->assertIsInt($result->insert_id);

        $user = $this->getUserByIdWithClassicBinding($result->insert_id);

        $this->assertObjectHasAttribute('userId', $user);
        $this->assertObjectHasAttribute('name', $user);
        $this->assertObjectHasAttribute('email', $user);
    }

    public function testCorrectQueriesWithCustomBinding()
    {
        $this->setCredentials();

        $params = [
            'email' => 'hello@mundo.com',
            'name' => 'hello'
        ];

        $result = $this->insertUserWithCustomBinding($params);

        $this->assertObjectHasAttribute('insert_id', $result);
        $this->assertObjectHasAttribute('affected_rows', $result);
        $this->assertIsInt($result->insert_id);

        $user = $this->getUserByIdWithCustomBinding(
            ['userId' => $result->insert_id]
        );

        $this->assertObjectHasAttribute('userId', $user);
        $this->assertObjectHasAttribute('name', $user);
        $this->assertObjectHasAttribute('email', $user);


        $this->setCredentials();

        $params = (object) [
            'email' => 'hello_object@mundo.com',
            'name' => 'hello_object',
            'otherProp' => 'foo',
            'otherPropNotUsed' => 'bar'
        ];

        $result = $this->insertUserWithCustomBinding($params);

        $this->assertObjectHasAttribute('insert_id', $result);
        $this->assertObjectHasAttribute('affected_rows', $result);
        $this->assertIsInt($result->insert_id);

        $user = $this->getUserByIdWithCustomBinding(
            ['userId' => $result->insert_id]
        );

        $this->assertObjectHasAttribute('userId', $user);
        $this->assertObjectHasAttribute('name', $user);
        $this->assertObjectHasAttribute('email', $user);
    }

    public function testInvalidParamsInPreparedQuery()
    {
        $this->setCredentials();

        $params = ['foo', 'bar@foo.com'];

        $this->expectException(InvalidArgumentException::class);

        $result = $this->insertUserWithCustomBinding($params);
    }

    public function testIncompleteParamsInPreparedCustomQuery()
    {
        $this->setCredentials();

        $params = [
            'name' => 'hello',
            'notEmail' => 'world@php.com'
        ];

        $this->expectException(InvalidArgumentException::class);

        $result = $this->insertUserWithCustomBinding($params);

        $params = (object) [
            'name' => 'hello',
            'notEmail' => 'world@php.com'
        ];

        $this->expectException(InvalidArgumentException::class);

        $result = $this->insertUserWithCustomBinding($params);
    }

    public function testWrongFormedQuery()
    {
        $this->setCredentials();

        $params = [
            'name' => 'myName',
            'email' => 'myEmail@php.com'
        ];

        $this->expectException(DatabaseException::class);
        
        $result = MySQLWrapper::query(
            // With not comma
            "INSER INTO users (name, email)
             VALUES ({name}, {email})",
            'ss',
            $params
        );
    }

    public function testRollbackTransaction()
    {
        $this->setCredentials();

        $usersNum = $this->getAllUsersNum();
        
        MySQLWrapper::startTransaction();

        $result = self::deleteAllUsers();
        
        $usersNumAfterDelete = $this->getAllUsersNum();

        // Users deleted
        $this->assertEquals($usersNumAfterDelete, 0);

        MySQLWrapper::rollbackTransaction();

        $finalUsersNum = $this->getAllUsersNum();

        $this->assertEquals($finalUsersNum, $usersNum);
    }

    public function testCommitTransaction()
    {
        $this->setCredentials();

        MySQLWrapper::startTransaction();

        $result = $this->insertUserWithCustomBinding(
            ['name' => 'Test commit', 'email' => 'Test email']
        );

        MySQLWrapper::commitTransaction();

        $user = $this->getUserByIdWithClassicBinding($result->insert_id);

        $this->assertEquals($user->userId, $result->insert_id);
    }

    public function testCommitOnNoTransaction()
    {
        $this->setCredentials();

        $this->expectException(DatabaseException::class);
           
        MySQLWrapper::commitTransaction();
    }

    public function testRollbackOnNoTransaction()
    {
        $this->setCredentials();

        $this->expectException(DatabaseException::class);
           
        MySQLWrapper::rollbackTransaction();
    }

    // If this test case is not at the final occurs some troubles with others
    public function testInvalidCredentials()
    {
        MySQLWrapper::$password = 'mariaPassIncorrect';   
        MySQLWrapper::$database = 'database';

        $this->expectException(DatabaseException::class);

        MySQLWrapper::getObjectBySql("SELECT 1 AS num");
    }
}
?>