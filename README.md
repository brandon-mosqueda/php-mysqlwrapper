# Getting started

MySQLWrapper provides an easy way to query MySQL and MariaDB databases. It's a higher layer of abstraction of [mysqli](https://www.php.net/manual/es/book.mysqli.php) class for PHP.

It was develop using PHP 7.3 and MariaDB 10.4.8. Check compatibility with older versions.

Include it in your project:

```php
require 'MySQLWrapper.php';
```

Set the credentials for connecting to your database, otherwise it will use the default credentials for MySQL. You only need to do it once everywhere in your application and you can modify the credentials after:

```php
// The default host is 'localhost'
MySQLWrapper::$host = 'your_host';

// The default username is 'root'
MySQLWrapper::$username = 'your_username';

// The default password is an empty string 
MySQLWrapper::$password = 'your_password';

// The default database is an empty string
MySQLWrapper::$database = 'your_database';

// The default port is 3306
MySQLWrapper::$port = 'your_port';
```

## Why use it?

To prevent SQL injections we will have to use something called prepared statements which uses bound parameters. Prepared Statements do not combine variables with SQL strings, so it is not possible for an attacker to modify the SQL statement. Prepared Statements combine the variable with the compiled SQL statement, this means that the SQL and the variables are sent separately and the variables are just interpreted as strings, not part of the SQL statement. [Sri at StackOverflow](https://stackoverflow.com/questions/24988867/when-should-i-use-prepared-statements).

### SQL injection

SQL injection is a code injection technique that might destroy your database, it is one of the most common web hacking techniques. It consits in the placement of malicious code in SQL statements, via web page input. Check more about that in [w3schools](https://www.w3schools.com/sql/sql_injection.asp).

### mysqli prepared or parameterized queries

In some versions of PHP exists a extension called _mysqli_ that allows you access to MySQL functionality through PHP from where you can execute queries, but it requires lots of code if you want to protect (or prepare) your queries, for example, using the example shown in the [oficial page of mysqli](https://www.php.net/manual/en/mysqli.quickstart.prepared-statements.php) it can be seen that you have to write some lines of code.

```php
$mysqli = new mysqli("my_host", "user", "password", "database");

if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

// Prepared statement, stage 1: prepare
if (!($stmt = $mysqli->prepare("INSERT INTO test(id) VALUES (?)"))) {
    echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
}

// Prepared statement, stage 2: bind and execute
$id = 1;
if (!$stmt->bind_param("i", $id)) {
    echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
}

if (!$stmt->execute()) {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
}

// explicit close recommended
$stmt->close();
```

The equivalent with MySQLWrapper should be something like that:

```php
require 'MySQLWrapper.php';

/*
  You only need to specify your credentials once and not in every ocation
  you query the database
 */
MySQLWrapper::$host = 'my_host';
MySQLWrapper::$username = 'user';
MySQLWrapper::$password = 'password';
MySQLWrapper::$database = 'database';

try {
    $id = 1;
    $result = MySQLWrapper::query(
                  "INSERT INTO test(id) VALUES (?)",
                  'i',
                  [$id]
              );

    /*
      You also can use this other form of parameterizing sending an assosiative
      array or an object and using its keys enclosed with curly braces in the 
      statement instead of using an interrogation mark and sending an ordered 
      array like the before example.
     */
    $params = [
        'id' => 1
    ];

    $result = MySQLWrapper::query(
                  "INSERT INTO test(id) VALUES ({id})",
                  'i',
                  $params
              );


} catch (DatabaseException $ex) {
  // Something is wrong with the statement or in the database
  echo $ex->getMessage(); // Get the error message
}
```

### More examples

```php
require 'MySQLWrapper.php';

/*
  You only need to specify your credentials once and not in every ocetion
  you query the database
 */
MySQLWrapper::$host = 'my_host';
MySQLWrapper::$username = 'user';
MySQLWrapper::$password = 'password';
MySQLWrapper::$database = 'database';

try {
    // If you do not need params in your query, it's not neccessary send
    // something to the functions.
    $users = MySQLWrapper::getArrayBySql("SELECT * FROM users");

    foreach ($users as $user) {
      // Do something with each user
      echo "The user {$user->name} has the id: {$user->userId}.";
    }

} catch (DatabaseException $ex) {
  // Something is wrong with the statement or in the database
  // You need to handle the exception
  echo $ex->getMessage();
}

try {
    $adminNum = MySQLWrapper::getCountBySql(
                    "SELECT COUNT(*) FROM users WHERE role = ?",
                    's',
                    ['admin']
                );

    echo "There are {$adminNum} administrators in the database";

} catch (DatabaseException $ex) {
  // You need to handle the exception
}

try {
    $user = MySQLWrapper::getObjectBySql(
                "SELECT * FROM users WHERE id = {id}",
                'i',
                ['id' => 1]
            );

    // Do whatever with the data
    echo "The user {$user->name} has the id {$user->id} ";

} catch (DatabaseException $ex) {
  // You need to handle the exception
}

try {
    /*
      Using this type of parameterizing the order of the params is not
      important.
     */
    $userToInsert = [
        'ageToInsert' => 26,
        'nameToInsert' => 'Yun Kennard',
        'emailToInsert' => 'kennard@domain.com'
    ];

    $result = MySQLWrapper::query(
                  "INSERT INTO users (name, email, age)
                   VALUES ({nameToInsert}, {emailToInsert}, {ageToInsert})",
                  'ssi',
                  $userToInsert
              );

    // Get the inserted id
    echo "The user was registered with the id: {$result->insert_id}";

} catch (DatabaseException $ex) {
  // You need to handle the exception
}
```

#### Transactions

A transaction is a logical unit of work that contains one or more SQL statements. When a transaction makes multiple changes to the database, either all the changes succeed when the transaction is committed, or all the changes are undone when the transaction is rolled back. Visit [w3resource](https://www.w3resource.com/mysql/mysql-transaction.php) for more information about transactions.

For managing transactions with MySQLWrapper show the examples below.

```php
try {
    // Start a transaction
    MySQLWrapper::startTransaction();


    /*
      Suppose you need to do multiple insertions, update other records or
      even delete another ones, but you need to ensure that all of the queries
      be executed successful or that none of them be executed, in this scenario
      you would use a transaction.
     */

    // Maybe an insertion
    // Maybe another insertion
    // Maybe multiples updates
    // And finally you delete some records

    // If there was no problem with the queries, you can save the changes
    // doing a commit of the transaction.
    MySQLWrapper::commitTransaction();  // Save the changes

} catch (DatabaseException $ex) {
    // Something went wrong and it is neccessary undo all changes in the
    // database. In this case you need to rollback the transaction
    MySQLWrapper::rollbackTransaction();  // Undo any number of changes
}
```
